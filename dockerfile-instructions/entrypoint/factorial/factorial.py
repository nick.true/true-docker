'''
Print factorial at a watchable pace.

usage: factorial.py <how-many>
'''
import time

index = 1
num   = 1

while True:
  num *= index
  index += 1
  print(num)
  time.sleep(1)