#!/bin/sh

echo "Waiting for signals to trap and printout..."

# catch and print out SIGINT, SIGQUIT, and SIGTERM 
# signals sent to this script
trap "echo SIGINT" INT
trap "echo SIGQUIT" QUIT
trap "echo SIGTERM" TERM

# wait for signals
while true; do
  sleep .5
done