'''This script runs a digital clock on the command line.

Usage:

python3 clock.py
'''

import time
import signal
import sys

def signalHandler(signal, frame):
  sys.exit(0)

def displayClock():
  sys.stdout.write("\n\t")
  sys.stdout.flush()

  while True:
    localtime = time.asctime( time.localtime(time.time()) )
    sys.stdout.write(localtime)
    sys.stdout.flush()
    time.sleep(1)
    sys.stdout.write('\b' * len(localtime)) # erase the time string

signal.signal(signal.SIGINT, signalHandler)
signal.signal(signal.SIGTERM, signalHandler)
displayClock()