import sys

from pyfiglet import figlet_format

msg = 'hello world!'
font = 'starwars'
argv = sys.argv
argc = len(argv)

if argc > 1:
  msg = argv[1]

if argc > 2:
  font = argv[2]

print(figlet_format(msg, font))