'''
This script waits for and prints out the signals that it receives.

usage: print-signals.py
'''

import signal
import time

def handler(sigNum, frame):
    print('Signal received: ', signal.Signals(sigNum).name)

for i in [s for s in dir(signal) if s.startswith("SIG")]:
  try:
    sigNum = getattr(signal,i)
    signal.signal(sigNum, handler)
  except (OSError, RuntimeError, ValueError) as e:
    print ("Skipping {}".format(i))

while True:
  time.sleep(1)